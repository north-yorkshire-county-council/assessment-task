﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

/// <summary>
/// Because the changes to the model overwrite the partial classes these customisations are added to prevent the loss
/// and maintain the customised functionality
/// </summary>
/// 

namespace WebApplication1.Models
{


    [MetadataType(typeof(ContactsMetaData))]
    public partial class Contacts
    {
    }

    public partial class ContactsMetaData
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "You need a first name")]
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public Nullable<System.DateTime> DateofBirth { get; set; }
        public string Gender { get; set; }
        public string eMailAddress { get; set; }
        public string VirtuallyDeleted { get; set; }
        public string Comments { get; set; }

        public virtual Contacts Contacts1 { get; set; }
        public virtual Contacts Contacts2 { get; set; }

    }

    //-------------

    [MetadataType(typeof(CardMetaData))]
    public partial class Card
    {
    }

    public partial class CardMetaData
    {
        public int Id { get; set; }

        [DisplayName("Card Value")]
        [Required(ErrorMessage = "You need a Card Value")]
        public Nullable<int> value { get; set; }

        [Required(ErrorMessage = "You need a Card Suit")]
        [DisplayName("Card Suit")]
        public string suit { get; set; }

    }


    public partial class Wordinator
    {
        public int Id { get; set; }
        public string originalText { get; set; }
        public string wordinatedText { get; set; }
    }

    public partial class Calculator
    {
        public int Id { get; set; }
        public string calculation { get; set; }

    }

    public partial class TextFile
    {
        public int Id { get; set; }
        public string Filepath { get; set; }
        public string FileContent { get; set; }

    }

    public partial class Record
    {
        public int Id { get; set; }
        public string RecordName { get; set; }
        public string RecordDetail { get; set; }
    }

    public partial class Mailer
    {
        public int id { get; set; }
        public string mailto { get; set; }
        public string mailfrom { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
    }
}



