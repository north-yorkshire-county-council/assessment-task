using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Linq.Expressions;
using System.IO;

using System.Net.Mail;
using System.Data;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View("About");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View("Contact");
        }

        public ActionResult Menu()
        {
            ViewBag.Message = "Your Menu page.";

            return View("Menu");
        }

        public ActionResult ContactListView()
        {

            var entities = new DevTeamTrainerEntities();

            return View("ContactListView", model: entities.Contacts);
         
        }

        public ActionResult ContactsEditView(int? id)
        {
            var entities = new DevTeamTrainerEntities();
            Contacts contact = entities.Contacts.Find(id);

            return View("ContactsEditView", contact);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContactsEditView(Contacts contact, string Command)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entities = new DevTeamTrainerEntities();
                    Contacts contacttobeupdated = entities.Contacts.Find(contact.ID);
                    contacttobeupdated.FirstName = contact.FirstName;
                 

                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return View("ViewExceptionError", new HandleErrorInfo(ex, "Home", "ContactListView"));
                }
                return RedirectToAction("ContactListView");
            }
            else
            {
                return RedirectToAction("ContactsListView");
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult CardCreateView()
        {
            return View("CardCreateView");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateAntiForgeryToken]
        public ActionResult CardCreateView(FormCollection fc, Card card, string ButtonPressed)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entities = new DevTeamTrainerEntities();

                    entities.Cards.Add(card);
                    entities.SaveChanges();

                    if (ButtonPressed == "Create")
                    {
                        return RedirectToAction("CardsListView");
                    }
                    else if (ButtonPressed == "Create Plus")
                    {
                        return RedirectToAction("CardCreateView");
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    return View("ViewExceptionError", new HandleErrorInfo(ex, "Home", "CardsListView"));
                }
            }
            else
            {
                return View("CardCreateView",card);
            }
        }

        public ActionResult CardDeleteView(int? id)
        {
            var entities = new DevTeamTrainerEntities();
            Card card = entities.Cards.Find(id);


            return View("CardDeleteView",card);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CardDeleteView(Card Card, string Command)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entities = new DevTeamTrainerEntities();
                    Card cardtobedeleted = entities.Cards.Find(Card.Id);

                    entities.Cards.Remove(cardtobedeleted);
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return View("ViewExceptionError", new HandleErrorInfo(ex, "Home", "CardsListView"));
                }
                return RedirectToAction("CardsListView");
            }
            else
            {
                return RedirectToAction("CardsListView");
            }
        }


        public ActionResult CardDetailsView(int? id)
        {
            var entities = new DevTeamTrainerEntities();
            Card card = entities.Cards.Find(id);

            return View("CardDetailsView",card);
        }



        public ActionResult CardEditView(int? id)
        {
            var entities = new DevTeamTrainerEntities();
            Card card = entities.Cards.Find(id);

            return View("CardEditView",card);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CardEditView(Card Card, string Command)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entities = new DevTeamTrainerEntities();
                    Card cardtobeupdated = entities.Cards.Find(Card.Id);
                    cardtobeupdated.value = Card.value;
                    cardtobeupdated.suit = Card.suit;

                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    return View("ViewExceptionError", new HandleErrorInfo(ex, "Home", "CardsListView"));
                }
                return RedirectToAction("CardsListView");
            }
            else
            {
                return RedirectToAction("CardsListView");
            }
        }




        /// <summary>
        /// This method gets the collection in Cards 
        /// and returns the collection as a 
        /// model to the CardsListView Page
        /// </summary>
        /// <returns></returns>
        public ActionResult CardsListView()
        {
            ViewBag.Message = "Your List of items in the Pack of Cards page.";

            var entities = new DevTeamTrainerEntities();

            return View("CardsListView",model: entities.Cards.OrderBy(p => p.suit).ThenBy(p => p.value));
        }

        /// <summary>
        /// This method has several examples of how search text could be used to natch records
        /// </summary>
        /// <param name="fc"></param>
        /// <param name="SearchText"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CardsListView(FormCollection fc, string SearchText)
        {
            ViewBag.Message = "Your List of items in the Pack of Cards page.";

            var entities = new DevTeamTrainerEntities();
            // get an empty list to put the matched ones in
            List<Card> matchedlist = new List<Card>();

            //this is the slowest probably if the list to be searched is a long list
            // but is easily modified to look at different typoes of match or matches in different columns
            if (SearchText.Contains("Method1"))
            {
                // turn the search text into a list of words
                string[] words = SearchText.Split(' ');

                // get a list of all the possible results
                List<Card> fulllist = entities.Cards.ToList();

                //for each item in the full possible list 
                foreach (Card c in fulllist)
                {
                    //for each word in the list of words
                    foreach (string w in words)
                    {  //check if this earch word matches 

                        // if it does match then add it to the list of matched ones 
                        if (c.suit == w)
                        {
                            matchedlist.Add(new Card()
                            {
                                Id = c.Id,
                                value = c.value,
                                suit = c.suit
                            });
                        }

                        // go the next search word
                    }
                    //go the next possible match
                }


            }

            // another way of doing it would be to build up a dynamic linq query
            else if (SearchText.Contains("Method2"))
            {


                IQueryable<Card> query = entities.Cards;


                if (!string.IsNullOrEmpty(SearchText))
                {
                    string[] splitList = SearchText.Split(' ');

                    query = query.Where(s => splitList.Any(st => s.suit.Contains(st)));
                }
                matchedlist = query.ToList();

            }

            // another way would be to have a stored procedure that we could pass values to

            // another way would be to build a dynamic string sql query 

            //each has speec and data consequences, each has complexity of the code involved, depends on the first example you find to copy from ...? 
            //no it needs to be a concious decision and tested perhaps several ways to find the best!
            // but this only worked for one word
            //

            //this one is the simplest and would work very well if there was a drop list instead of the text box and only one word in a
            //finite or slow moving dimension list
            else
            {
                return View("CardsListView",model: entities.Cards.Where(p => p.suit.Contains(SearchText)));

            }

            //return a model of those ones (however and whichever way we found them) that made it to the list of matched ones
            return View("CardsListView",matchedlist);
        }


        public ActionResult RecordViewBagView()
        {
            Record rec = new Record
            {
                Id = 101,
                RecordName = "Mark",
                RecordDetail = " the world is grey"
            };
            ViewBag.Message = rec;
            return View("RecordViewBagView");
        }

        public ActionResult RecordViewDataView()
        {
            Record rec = new Record
            {
                Id = 101,
                RecordName = "Wendy",
                RecordDetail = " the world is blue"
            };
            ViewData["Message"] = rec;
            return View("RecordViewDataView");
        }


        public ActionResult WordinatorView()
        {
            Wordinator rec = new Wordinator
            {
                Id = 101,
                originalText = "",
                wordinatedText = ""
            };
            ViewData["Message"] = rec;
            return View("WordinatorView");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WordinatorView(FormCollection fc, Wordinator formfields)
        {


            string originaltext = formfields.originalText;
            string wordinatedtext = "";

            // split the originaltext into an array of single words


            string[] words = originaltext.Split(' ');

            foreach (string w in words)
            {
                string middlebit = "";


                if (w.Length <= 3)
                {
                    wordinatedtext = wordinatedtext + @" " + NYCC.Mark.CommonFunctions.ReverseString(w);
                }
                else if (w.Length > 3)
                {
                    middlebit = NYCC.Mark.CommonFunctions.Middle(w, 1, w.Length - 2);

                    wordinatedtext = wordinatedtext + @" "
                            + NYCC.Mark.CommonFunctions.Left(w, 1) + NYCC.Mark.CommonFunctions.ReverseString(middlebit)
                            + NYCC.Mark.CommonFunctions.Right(w, 1);
                }
            }

            //send the wordinated text back to the form
            Wordinator rec = new Wordinator()
            {
                Id = 101,
                originalText = formfields.originalText,
                wordinatedText = wordinatedtext
            };
            ViewData["Message"] = rec;
            return View("WordinatorView");
        }


        public ActionResult CalculatorView()
        {
            Calculator rec = new Calculator()
            {
                Id = 101,
                calculation = "0",
            };


            //Calculator mysum = new Calculator();
            //mysum.Id = 34;
            //mysum.calculation = "6*5";








            ViewData["Message"] = rec;
            return View("CalculatorView");
        }




        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CalculatorView( string calculation, string Activity)
        {

            string updatedcalculation = "";


            if (Activity == "=")
            {
                try
                {
                    double result;
                    result = Convert.ToDouble(new DataTable().Compute(calculation, null));
                    updatedcalculation = result.ToString();
                }
                catch (Exception e)
                {
                    updatedcalculation = "Error!";// + e.Message;
                }

            }
            else if (Activity == "C")
            {
                updatedcalculation = "0";
            }
         
            else
            {
                if (calculation == "0")
                {
                    updatedcalculation = Activity;
                }
                else if (calculation == "Error!")
                {
                    updatedcalculation = Activity;
                }
                else if (calculation == "Infinity")
                {
                    updatedcalculation = Activity;
                }
                else
                { updatedcalculation = calculation + Activity; }
            }



            Calculator rec = new Calculator()
            {
                Id = 101,
                calculation = updatedcalculation

            };
            ViewData["Message"] = rec;
            return View("CalculatorView");
        }











        public ActionResult WordProcessor()
        {
            ViewBag.Message = "Word Processor.";
            TextFile tf = new TextFile
            {
                Id = 0,
                FileContent = "",
                Filepath = ""

            };


            ViewData["message"] = tf;
            return View("WordProcessor");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WordProcessor(TextFile textFile, string Action)
        {

            if (Action == "Save")
            {




                string mydocpath;
                string fn; ;
                string txt;



                fn = textFile.Filepath;
                txt = textFile.FileContent;


                // Create a string array with the lines of text
                //   string[] lines = { "First line", "Second line", "Third line" };

                // Set a variable to the My Documents path.

                if (string.IsNullOrEmpty(fn))
                {
                    fn = "default.txt";
                }

               


                mydocpath =
                Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);



                System.IO.File.WriteAllText(Path.Combine(mydocpath, fn), txt.ToString());

                ViewData["message"] = textFile;
                return View("WordProcessor");

            }

            else if (Action == "Read File")
            {
                int counter = 0;
             
                string fn = textFile.Filepath;
                //// Read the file and display it line by line.  
                //StreamReader file =
                //    new System.IO.StreamReader(fn);
                //while ((line = file.ReadLine()) != null)
                //{
                //    //Console.WriteLine(line);
                //    textFile.FileContent = textFile.FileContent + line;
                //    counter++;
                //}

                //file.Close();


                             

               
                // Read each line of the file into a string array. Each element
                // of the array is one line of the file.
                string[] lines = System.IO.File.ReadAllLines(fn);

                // Display the file contents by using a foreach loop.
            
                foreach (string line in lines)
                {

                    textFile.FileContent = textFile.FileContent + @"<br />" + line ;
                }



                ViewData["message"] = textFile;
                return View("WordProcessor");

            }
            else
            {
                return View("WordProcessor");
            }
        }

            public ActionResult ViewMailer()
        {
            Mailer rec = new Mailer
            {
                id = 101,
                body = "",
                mailfrom = "",
                mailto = "",
                subject = ""

            };
            ViewData["Message"] = rec;
            return View("ViewMailer");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ViewMailer(FormCollection fc, Mailer m)
        {

            //declarations
            SmtpClient client;
            MailMessage message;

            //Find out the email addresses and 
            //server details to send a message from the database

            client = new SmtpClient("mail.county.nycc.internal")
            {
                Credentials = new System.Net.NetworkCredential("", ""),
                Port = 25
            };


            message = new MailMessage(m.mailfrom.ToString(), m.mailto.ToString())
            {
                Subject = m.subject.ToString(),
                Body = m.body.ToString()
            };

            // Send the message.
            client.Send(message);


            //navigate
            ViewBag.Message = "Your Mailer page.";

            return View("ViewMailer");
        }


        public ActionResult PasswordAssessorView()
        {
            return View("PasswordAssessorView");
        }
    }
}