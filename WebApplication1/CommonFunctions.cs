﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYCC.Mark
{
    class CommonFunctions
    {

        public static string Left(string FullText, int NumberOfCharacters)
        {
            var result = "";
            if (string.IsNullOrEmpty(FullText)) return result;
            NumberOfCharacters = Math.Abs(NumberOfCharacters);

            result = (FullText.Length <= NumberOfCharacters
            ? FullText
            : FullText.Substring(0, NumberOfCharacters)
            );

            return result;

            


        }
        public static string Right(string FullText, int NumberOfCharacters)
        {
            var result = "";
            if (string.IsNullOrEmpty(FullText)) return result;
            NumberOfCharacters = Math.Abs(NumberOfCharacters);

            result = (FullText.Length <= NumberOfCharacters
             ? FullText
             : FullText.Substring(FullText.Length - NumberOfCharacters, NumberOfCharacters)
             );
            return result;
        }

        public static string Middle(string FullText, int StartPosition, int NumberOfCharacters)
        {
            string result = "";
            if (((FullText.Length <= 0) || (StartPosition >= FullText.Length))) return result;
            if ((StartPosition + NumberOfCharacters > FullText.Length))
            {
                NumberOfCharacters = (FullText.Length - StartPosition);
            }
            result = FullText.Substring(StartPosition, NumberOfCharacters);
            return result;
        }

        public static string ReverseString(string str)
        {

            char[] chars = str.ToCharArray();

            for (int i = 0, j = str.Length - 1; i < j; i++, j--)

            {

                char c = chars[i];

                chars[i] = chars[j];

                chars[j] = c;

            }

            return new string(chars);

        }

    }




}

