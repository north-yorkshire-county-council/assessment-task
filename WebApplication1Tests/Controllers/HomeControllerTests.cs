﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApplication1.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using WebApplication1.Models;
using System.Linq.Expressions;
using System.Net.Mail;
using System.Data;


namespace WebApplication1.Controllers.Tests
{
    [TestClass()]
    public class HomeControllerTests
    {
        [TestMethod()]
        public void IndexTest1()
        {
            var controller = new HomeController();
            var result = controller.Index() as ViewResult;

            Assert.AreEqual("Index", result.ViewName);

        }


        [TestMethod()]
        public void MenuTest()
        {
            var controller = new HomeController();
            var result = controller.Menu() as ViewResult;
            Assert.AreEqual("Menu", result.ViewName);

        }
        [TestMethod()]
        public void CalculatorViewTest1()
        {
            var controller = new HomeController();
            var result = controller.CalculatorView() as ViewResult;
            Assert.AreEqual("CalculatorView", result.ViewName);
        }

        [TestMethod()]
        public void CalculatorViewTest2()
        {
            var controller = new HomeController();
            var result = controller.CalculatorView("10+6-3", "=") as ViewResult;

            Calculator exp = new Calculator()
            {
                Id = 101,
                calculation = "13"
            };


            ViewDataDictionary viewData = result.ViewData;
            Calculator res = (Calculator)viewData["Message"];



            Assert.AreEqual(exp.calculation, res.calculation);

        }


        [TestMethod()]
        public void CalculatorViewTest3()
        {
            var controller = new HomeController();

            List<string> calcs = new List<string>();

            calcs.Add("1+4+6-7*4");
            calcs.Add("9*9+3");
            calcs.Add("7/2");



            List<string> answers = new List<string>();

            answers.Add("-17");
            answers.Add("84");
            answers.Add("3.5");

            bool allok = true;

            for (int i = 0; i <= 2; i++)
            {


                var result = controller.CalculatorView(calcs[i], "=") as ViewResult;
                ViewDataDictionary viewData = result.ViewData;
                Calculator res = (Calculator)viewData["Message"];

                Calculator exp = new Calculator()
                {
                    Id = 101,
                    calculation = answers[i]
                };
                if (exp.calculation != res.calculation)
                {
                    allok = false;
                }
            }
            Assert.IsTrue(allok);
        }

        

            
            



          

        

    }
}